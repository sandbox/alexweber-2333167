; Drush Make file
;
; Use this file to build a full distribution including Drupal core and the
; "Panopoly Commerce" distribution using the following command...
;
; drush --no-patch-txt make build-panopoly_commerce.make <target directory>

api = 2
core = 7.x

; Include the definition for how to build Drupal core directly, including
; patches.

includes[] = drupal-org-core.make

; Download the Panopoly Commerce and recursively build all
; of its dependencies.

projects[panopoly_commerce][type] = profile
; The following two lines allow for pulling the distribution from the drupal
; git repository;
projects[panopoly_commerce][download][type] = git
projects[panopoly_commerce][download][branch] = 7.x-1.x
; You can optionally build from a local directory using the make_local drush
; module found at http://drupal.org/project/make_local.
;projects[panopoly_commerce][download][type] = local
;projects[panopoly_commerce][download][source] = /Users/alex/Desktop/tmp/panopoly_commerce
